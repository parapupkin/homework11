<?php
namespace myNamespace\classes;   

class Order 
{ 
    private $id;
    private static $counter = 0;

    // считаем, сколько всего товаров в корзине
    public function quantity(Cart $Cart) 
    {
        $quantity = 0;
        foreach($Cart->items as $cartItem) $quantity += $cartItem->quantity;
        echo "Итого количиство едениц товаров: " . $quantity . '<br>';        
    }
    // оформляем заказ
    public function checkout(Cart $Cart) 
    {
        $total = 0;
        foreach($Cart->items as $cartItem) $total += $cartItem->subtotal;
        echo "Вы оформили заказ на сумму: " . $total . '<br>';         
    }   
    
    function __construct()
    {
        self::$counter++;
        $this->id=self::$counter;
    }

    function getId()
    {
        return $this->id;
    }
     // выводим заказ на печать
    public function printOrder() 
    {
        echo "Я распечатал заказ №{$this->getId()} <br>";
    }

}

?>