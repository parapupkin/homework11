<?php
    namespace myNamespace\classes;    
    abstract class Product 
    {
        public $name;
        public $category;         
        protected $price;        
        public $quantity = 1; //количество единиц товара
        public $subtotal = 0; //сумма за товар с учетом его количества                  

        public function __construct($name, $category, $price, $quantity)
        {
            $this->name = $name;
            $this->category = $category;
            $this->price = $price;
            $this->quantity = $quantity;
            // echo "Вы покупаете товар:  $this->name. ";
        }        
        public function subtotal() {
        $this->subtotal = $this->price*$this->quantity;
        return $this->subtotal;
        }

        abstract public function getDescription();
        // use \myNamespace\traits\GetPrice; - просто проверял, работает ли автозагрузка трейтов :)
    } 

  ?>