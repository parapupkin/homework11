<?php
namespace myNamespace\classes;   

class Cart {
    
    public $items=[];

    // добавляем товар в корзину. Если в ней уже есть товар с таким же name, просто меняем его количество
    public function add(Product $cartItem) {
        if(isset($this->items[$cartItem->name])) {
            $cartItem->quantity += $this->items[$cartItem->name]->quantity;
        }
        $this->edit($cartItem);
    }

    // редактируем товар в корзине, либо добавляем новый (кол-во не суммируется, для суммирования используется метод add(Product $cartItem))
    public function edit(Product $cartItem) {
        $cartItem->subtotal();
        $this->items[$cartItem->name] = $cartItem;
    }
    // функция удаления элемента из корзины по имени
    public function delete($name) {
        unset($this->items[$name]);
    }
    // функция очищеня корзины
    public function emptyCart() {
        $this->items=[];
    }    
}

?>