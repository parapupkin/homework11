<?php
    trait GetDiscountedPrice 
    {           
        public function getDiscountedPrice() 
        {   
            $this->discount = 10;
            $this->price = round(($this->cost - $this->cost*$this->discount/100), 2);
            echo "Цена товара со скидкой {$this->price} руб.";
        }        
    }    

  ?>