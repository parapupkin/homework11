<?php
    namespace myNamespace\traits;
    trait GetPriceOnWeight 
    {           
        public function GetPriceOnWeight() 
        {
            if ($this->weight > 10) {
            $this->discount = 10;
            $this->price = round(($this->cost - $this->cost*$this->discount/100), 2);
             echo "Цена товара со скидкой  {$this->price} руб. ";
            } else {
            $this->discount = 0;
             echo "Цена товара {$this->cost} руб. Возьмите больше 10 кг и получите скидку. ";
            }           
        }        
    }

  ?>