<?php   
    function myAutoloadClasses($className) 
    {
        $filePath = __DIR__ . DIRECTORY_SEPARATOR . str_replace("\\", DIRECTORY_SEPARATOR, $className) . '.class.php'; 
        if (file_exists($filePath)) {
        include "$filePath";
        } 
    }
    function myAutoloadInterfaces($interfaceName) 
    {
        $filePath = __DIR__ . DIRECTORY_SEPARATOR . str_replace("\\", DIRECTORY_SEPARATOR, $interfaceName) . '.interface.php'; 
        if (file_exists($filePath)) {
        include "$filePath";
        } 
    }
    function myAutoloadTraits($traitName) 
    {
        $filePath = __DIR__ . DIRECTORY_SEPARATOR . str_replace("\\", DIRECTORY_SEPARATOR, $traitName) . '.trait.php'; 
        if (file_exists($filePath)) {
        include "$filePath";
        } else {
        die("Объекта $traitName не существует");
        }
    }
    spl_autoload_register('myAutoloadClasses');
    spl_autoload_register('myAutoloadInterfaces');
    spl_autoload_register('myAutoloadTraits');    

    // изначально было вместо __DIR__  -  $_SERVER['DOCUMENT_ROOT'], но на хостинге не работало, поэтому заменил
?> 